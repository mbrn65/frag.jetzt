export const material = {
  "--primary": "#4caf50",
  "--primary-variant": "#087f23",

  "--secondary": "#ff5722",
  "--secondary-variant": "#c41c00",

  "--background": "#263238",
  "--surface": "#4f5b62",
  "--dialog": "#37474f",
  "--cancel": "#26343c",
  "--alt-surface": "#323232",
  "--alt-dialog": "#455a64",

  "--on-primary": "#000000",
  "--on-secondary": "#000000",
  "--on-background": "#FFFFFF",
  "--on-surface": "#FFFFFF",
  "--on-cancel": "#FFFFFF",

  "--green": "lightgreen",
  "--red": "red",
  "--yellow": "yellow",
  "--blue": "#3f51b5",
  "--purple": "#9c27b0",
  "--light-green": "#80ba24",
  "--grey": "#BDBDBD",
  "--grey-light": "#9E9E9E",
  "--black": "#212121",
  "--moderator": "#37474f",
};

export const material_meta = {
  translation: {
    name: {
      en: "Not so Dark",
      de: "Not so Dark",
    },
    description: {
      en: "Not so dark, kind of battery-saving background",
      de: "Nicht so dunkler, etwas akkuschonender Hintergrund",
    },
  },
  isDark: true,
  order: 4,
  scale_desktop: 1,
  scale_mobile: 1,
  previewColor: "background",
};
