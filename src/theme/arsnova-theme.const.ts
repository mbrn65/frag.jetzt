import { dark, dark_meta } from "./dark-theme/darkTheme.const";
import { arsnova, arsnova_meta } from "./light-theme/light-theme";
import { blue } from "./blue-theme/blueTheme.const";
import { purple, purple_meta } from "./purple-theme/purpleTheme.const";
import {
  highcontrast,
  highcontrast_meta,
} from "./high-contrast-theme/highContrastTheme.const";
import { material, material_meta } from "./material-theme/materialTheme.const";

export const themes = {
  arsnova: arsnova,
  dark: dark,
  projector: purple,
  highcontrast: highcontrast,
  material: material,
};

export const themes_meta = {
  arsnova: arsnova_meta,
  dark: dark_meta,
  projector: purple_meta,
  highcontrast: highcontrast_meta,
  material: material_meta,
};
